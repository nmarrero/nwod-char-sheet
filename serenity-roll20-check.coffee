roll20Security = [
	/(\bdata:\b|eval|cookie|\bwindow\b|\bparent\b|\bthis\b)/i, 
	/behaviou?r|expression|moz-binding|@import|@charset|(java|vb)?script|[\<]|\\\w/i, 
	/[\x7f-\xff]/, 
	/[\x00-\x08\x0B\x0C\x0E-\x1F]/, 
	/&\#/
]
$.get( "lib/serenity.css", ( data ) ->
  console.log 'Received css. Running roll20 checks...'
  testResults( data );
  console.log 'Finished checking css'
  )

testResults = ( data ) ->
	cssToTest = data.split(/\r\n|\r|\n/)
	for line, index in cssToTest
		for test in roll20Security
			console.log "#{index}: Found #{test} in #{line}" if line.match( test )					
								