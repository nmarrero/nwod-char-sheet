window.serenity ?= {}
window.serenity.SKILLS = [
    'Animal Handling'
    'Artistry'
    'Athletics'
    'Covert'
    'Craft'
    'Discipline'
    'Guns'
    'Heavy Weapons'
    'Influence'
    'Knowledge'
    'Linguist'
    'Mechanical Engineering'
    'Medical Expertise'
    'Melee Weapons'
    'Perception'
    'Performance'
    'Pilot'
    'Planetary Vehicles'
    'Ranged Weapons'
    'Scientific Expertise'
    'Survival'
    'Technical Engineering'
    'Unarmed Combat'
  ]
  
window.serenity.ASSETS = [
    {
    	name: "Allure" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 on any socializing skill rolls"
    }
	{
    	name: "Allure" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Related plot points spent increased by 2"
    }
	{
    	name: "Athlete" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 to Athletic skill rolls"
    }
	{
    	name: "Athlete" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Related plot points spent increased by 2"
    }
	{
    	name: "Cortex Specter" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Difficulty to find info on you in the Cortex is +8"
    }
	{
    	name: "Cortex Specter" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You don't exist in the Cortex at all"
    }
	{
    	name: "Fightin' Type" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You can take one non-combat action each turn without penalty"
    }
	{
    	name: "Friends in High Places" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Spend plot points for a favor: 1/2 - 500 credit loan/minor equipment 3/4 - 5000 credit loan/lift a land lock/important event invitation 5/6 - 10000 credit loan/security clearance/use of a ship"
    }
	{
    	name: "Friends in Low Places" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Spend plot points for a favor: 1/2 - 500 credit loan (with interest)/information/imprinted goods 3/4 - 5000 credit loan (with interest)/a cut on a smuggling job 5/6 - 10000 credit loan (with interest)/protection from rival crime lord"
    }
	{
    	name: "Good Name" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 in social skills where your name/reputation is involved"
    }
	{
        name: "Good Name" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+4 in social skills where your name/reputation is involved"
    }
	{
    	name: "Healthy as a Horse" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to resist poison or disease"
    }
	{
    	name: "Healthy as a Horse" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+4 bonus to resist poison or disease"
    }
	{
    	name: "Heavy Tolerance" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to resist alcohol drugs gas or poison"
    }
	{
    	name: "Highly Educated" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to any knowledge-based skill role"
    }
	{
    	name: "Intimidatin' Manner" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to intimidation interrogation bullying etc."
    }
	{
    	name: "Leadership" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Once per session +2 bonus to team on any one action relating to a pre-designated goal"
    }
	{
    	name: "Leadership" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You may also apply plot points to improve team actions relating to the same goal"
    }
	{
    	name: "Lightnin' Reflexes" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to Initiative"
    }
	{
    	name: "Math Whiz" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 to math accounting engineering science etc. Most math solved without roll"
    }
	{
    	name: "Mean Left Hook" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Unarmed attacks inflict Basic damage (split between Stun and Wound)"
    }
	{
    	name: "Mechanical Empathy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Spend plot points to get intuitive understanding of problem with mechanical device (split between minor/moderate/major). +2 bonus to fix"
    }
	{
    	name: "Moneyed Individual" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Starting credits * 1.5 Can perform a specialty roll (e.g. Intelligence+Influence) once per session to dip into trust to make purchase at difficulty of 3 for up to 2000 credits adding 4 for every 2000 credits more. Can spend plot points on roll."
    }
	{
    	name: "Natural Linguist" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to social skills when trying to pass as a native interpret other languages/dialects. Linguistic specialties cost half."
    }
	{
    	name: "Nature Lover" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to all applicable skills in a natural setting (e.g. alertness or survival)"
    }
	{
    	name: "Nose for Trouble" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Can roll to sense trouble at any time. +2 bonus to Intelligence or Alertness when circumstances warrant"
    }
	{
    	name: "Nose for Trouble" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Can spend 1 plot point to negate all effects of surprise"
    }
	{
    	name: "Reader" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to Alertness when trying to empathically understand a person (e.g. observation discerning truth form a lie etc.)"
    }
	{
    	name: "Reader" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+4 bonus to Alertness when trying to empathically understand a person (e.g. observation discerning truth form a lie etc.) can spend plot points once per session to gain extra clues (minor/moderate/major info)"
    }
	{
    	name: "Registered Companion" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to Influence-based actions in dealing with those who respect your station"
    }
	{
    	name: "Steady Calm" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to willpower check to avoid shaken/frighten"
    }
	{
    	name: "Steady Calm" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Never startled rattled shaken or afraid (except under external circum like drugs)"
    }
	{
    	name: "Sweet and Cheerful" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 to positive social interactions (i.e. not intimidate)"
    }
	{
    	name: "Things go Smooth" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "1 reroll/session on any roll except for Botches except where noted by GM (major dramatic failures that work with the plot)"
    }
	{
    	name: "Things go Smooth" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "2 rerolls/session on any roll (even Botches) except where noted by GM (major dramatic failures that work with the plot)"
    }
	{
    	name: "Total Recall" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 bonus to any knowledge skill (you never forget details you've experienced). You may spend a plot point to remember verbatim or with photographic calrity"
    }
	{
    	name: "Tough as Nails" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You gain 2 extra life points"
    }
	{
    	name: "Tough as Nails" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You gain 4 extra life points"
    }
	{
    	name: "Two-fisted" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Ambidextrous No off-hand penalty"
    }
	{
    	name: "Walking Timepiece" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Always knows time and elapsed time (human stopwatch). If incapacitated roll a full-turn Intelligence+Alertness at Average difficulty to get internal clock ticking again"
    }
	{
    	name: "Wears a Badge" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Your authority covers most of the system"
    }
	{
    	name: "Born Behind the Wheel" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "+2 to pilot rolls for land or air/space"
    }
	{
    	name: "Born Behind the Wheel" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Related plot points spent increased by 2"
    }
	{
    	name: "Military Rank" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Enlisted: +2 bonus on all Discipline-based actions. Officer: +2 bonus on all Influence-based actions"
    }
	{
    	name: "Religiosity" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "(follower of chosen religion) +2 bonus to Willpower once per session"
    }
	{
    	name: "Religiosity" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "(teacher of chosen religion) Related plot points spent increased by 2 with those who respect your station"
    }
	{
    	name: "Sharp Sense" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "+2 bonus to alertness/perception for chosen sense"
    }
	{
    	name: "Talented" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "+2 bonus to chosen skill specialty in all situations"
    }
	{
    	name: "Talented" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Each progression to higher die costs 2 less than normal during advancement"
    }
	{
    	name: "Wears a Badge" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "+2 to Influence-based actions when dealing with those who respect your position in chosen region or planet"
    }
	{
    	name: "Ain't Got Time to Bleed" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-1 reduction in wound penalty"
    }
	{
    	name: "Ain't Got Time to Bleed" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 reduction in wound penalty"
    }
	{
    	name: "Allure" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 on any socializing skill rolls"
    }
	{
    	name: "Athlete" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 to Athletic skill rolls"
    }
	{
    	name: "Blue Blood" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 to influence to deal with red tape 'Verse wide"
    }
	{
    	name: "Born in the Black" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 0g +2 vacc +1 steward -1 all dirtside activities"
    }
	{
    	name: "Comp. Tech" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to Comms Sensors Computers"
    }
	{
    	name: "Comp. Tech" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to Comms Sensors Computers"
    }
	{
    	name: "Fast on Your Feet" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "1.5x Movement Speed"
    }
	{
    	name: "Fast on Your Feet" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "2x Movement Speed"
    }
	{
    	name: "Friends in High Places" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 in 'proper' social standing rolls (restaurant club) to acquire temporary assets/influence"
    }
	{
    	name: "Friends in High Places" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 in 'proper' social standing rolls (restaurant club) to acquire temporary assets/influence"
    }
	{
    	name: "Friends in Low Places" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 in 'proper' social standing rolls (back alley dive) to acquire temporary assets/influence"
    }
	{
    	name: "Friends in Low Places" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 in 'proper' social standing rolls (back alley dive) to acquire temporary assets/influence"
    }
	{
    	name: "Heavy Tolerance" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to resist alcohol drugs gas or poison"
    }
	{
    	name: "Highly Educated" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to any knowledge-based skill role"
    }
	{
    	name: "Home on the Range" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to riding vet anything with animals"
    }
	{
    	name: "Home on the Range" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to riding vet anything with animals"
    }
	{
    	name: "Influential Suitor" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Someone of substance wants to marry you and is constantly pursuing you"
    }
	{
    	name: "In Plain Sight" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to hide stealth disguise avoid detection"
    }
	{
    	name: "In Plain Sight" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to hide stealth disguise avoid detection"
    }
	{
    	name: "Intimidatin' Manner" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to intimidation interrogation bullying etc."
    }
	{
    	name: "Leadership" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Once per session +4 bonus to team on any one action relating to a pre-designated goal"
    }
	{
    	name: "Light Sleeper" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to detection if someone is sneaking up on you while asleep"
    }
	{
    	name: "Lightnin' Reflexes" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to Initiative"
    }
	{
    	name: "Lightnin' Reflexes" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to Initiative"
    }
	{
    	name: "Math Whiz" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 to math accounting engineering science etc. Most math solved without roll"
    }
	{
    	name: "Mean Left Hook" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Forces an endurance check on the target with a successful unarmed hit"
    }
	{
    	name: "Mechanical Empathy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to diagnose problems (choose mechanical or electronic/computer"
    }
	{
    	name: "Mechanical Empathy" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to diagnose problems (choose mechanical or electronic/computer"
    }
	{
    	name: "Middleman" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Skills in finding things/people for a single planet and resource"
    }
	{
    	name: "Middleman" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Skills in finding things/people system-wide and multiple resources"
    }
	{
    	name: "Mighty Fine Hat" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Known for an item that's always with you (hat gun jacket etc)"
    }
	{
    	name: "Nature Lover" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to all applicable skills in a natural setting (e.g. alertness or survival)"
    }
	{
    	name: "Nose for Trouble" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to rolls to determine surprise"
    }
	{
    	name: "Nose for Trouble" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus to rolls to determine surprise"
    }
	{
    	name: "On Retainer" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Your prior service pays you for the option to consult you in the future access to minor income GM tool"
    }
	{
    	name: "Religiosity" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "(follower) +2 Willpower skills in crisis"
    }
	{
    	name: "Religiosity" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "(teacher) +4 +2 in social skills/influence involving the religion"
    }
	{
    	name: "Renaissance Man" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Jack of all Trades +1"
    }
	{
    	name: "Renaissance Man" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Jack of all Trades +2"
    }
	{
    	name: "Sawbones" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2\tbonus to medicine and medical skills"
    }
	{
    	name: "Sawbones" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4\tbonus to medicine and medical skills"
    }
	{
    	name: "Teacher" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Instruction +1"
    }
	{
    	name: "Teacher" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Instruction +2"
    }
	{
    	name: "Total Recall" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus to any knowledge skill (you never forget details you've experienced)"
    }
	{
    	name: "Tough as Nails" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Resist 1 melee dmg"
    }
	{
    	name: "Tough as Nails" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Resist 2 melee dmg"
    }
	{
    	name: "Trustworthy Face" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus on rolls to convince someone to trust you"
    }
	{
    	name: "Trustworthy Face" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus on rolls to convince someone to trust you"
    }
	{
    	name: "Trustworthy Gut" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 bonus on 'hunch' rolls ('hunch' rolls are when the player rolls but isn't sure just 'feels' like something is up like a willpower roll to counter someone's persuade because you think they're full of ****"
    }
	{
    	name: "Trustworthy Gut" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+4 bonus on 'hunch' rolls ('hunch' rolls are when the player rolls but isn't sure just 'feels' like something is up like a willpower roll to counter someone's persuade because you think they're full of ****"
    }
	{
    	name: "Two-fisted" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Ambidextrous No penalty for either hand may wield 2 weapons with a reduced penalty"
    }
	{
    	name: "Walking Timepiece" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Always knows time and elapsed time (human stopwatch)"
    }
	{
    	name: "Cortex Specter" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: true
    	original: false
    	description: "Difficulty to find info on you in the Cortex is +8"
    }
	{
    	name: "Cortex Specter" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: true
    	original: false
    	description: "You don't exist in the Cortex at all"
    }
	{
    	name: "Reader (psionic > 8)" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: true
    	original: false
    	description: "+2 bonus to psion talent rolls"
    }
	{
    	name: "Reader (psionic > 8)" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: true
    	original: false
    	description: "+4 bonus to psion talent rolls"
    }
	{
    	name: "Wears a Badge" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: true
    	original: false
    	description: "+2 to social influence in region (local law enforcement)"
    }
	{
    	name: "Wears a Badge" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: true
    	original: false
    	description: "+4 to social influence in region (system law enforcement)"
    }
	{
    	name: "Blue Blood" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "+2 to influence to deal with red tape in a single chosen system"
    }
	{
    	name: "Born Behind the Wheel" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "+4 to pilot rolls for land or air/space"
    }
	{
    	name: "Sharp Sense" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "+4 bonus to alertness/perception for chosen sense"
    }
	{
    	name: "Talented" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "+1 bonus to chosen skill in all situations"
    }
	{
    	name: "Talented" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "+2 bonus to chosen skill in all situations"
    }
]

window.serenity.COMPLICATIONS = [
    {
    	name: "Amorous (over sexed)" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to influence and -2 to willpower to resist wiles of your 'type'"
    }
	{
    	name: "Bleeder" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Require medical help (hard Intelligence+Medical Expertise action) when taking Wounds or will bleed out 1 additional Wound each turn"
    }
	{
    	name: "Blind" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-4 to any skill that requires vision -8 to ranged combat cannot read non-blind friendly screens/displays. Gain Sharp Senses (touch and hearing) for free"
    }
	{
    	name: "Branded" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to social skills where your reputation comes into play"
    }
	{
    	name: "Branded" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Everyone knows your reputation (some sympathize however)"
    }
	{
    	name: "Chip On Your Shoulder" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Anger management issues -2 to peaceable social actions with tension"
    }
	{
    	name: "Chip On Your Shoulder" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Anger management issues Berserk when Wounded focusing only on the one who wounded you"
    }
	{
    	name: "Combat Paralysis" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Unable to fight in the first d2 rounds of a fight (can spend plot points to shake off)"
    }
	{
    	name: "Combat Paralysis" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Unable to fight in the first d4 rounds of fight cannot spend plot points to shake off"
    }
	{
    	name: "Coward" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to combat actions when you're in danger -2 to Willpower to resist fear intimidation torture or other threats"
    }
	{
    	name: "Crude" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to social skills to influence in 'proper' settings"
    }
	{
    	name: "Dead Broke" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Half starting credits which must be spent immediately. You must give up half your income every time you're in port to pay off your debts"
    }
	{
    	name: "Deadly Enemy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "GM tool plays into background"
    }
	{
    	name: "Deaf" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Auto-fail any skill/alertness that requires hearing immune to sonic attacks +2 to read lips"
    }
	{
    	name: "Easy Mark" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-4 to rolls to avoid being persuaded -4 to resistances to being persuaded (i.e. willpower) gullible"
    }
	{
    	name: "Ego Signature" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You always leave a token or mark behind. Easily framed easier to track. GM tool."
    }
	{
    	name: "Filcher" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Kleptomaniac - Compelled to steal anything that catches your eye"
    }
	{
    	name: "Forked Tongue" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You lie all the time. Compelled to not tell the truth -4 to influence rolls due to your reputation"
    }
	{
    	name: "Greedy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You will take almost any opportunity to acquire money"
    }
	{
    	name: "Hooked" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Not overly dangerous (cigarettes functioning alcoholic) -2 to all attributes for a week until you get your next dose (daily)"
    }
	{
    	name: "Hooked" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Abusing a dangerous substance -4 to all attributes for 2 weeks until you get your next dose (daily)"
    }
	{
    	name: "Leaky Brainpan" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to social skill rolls you're distracted"
    }
	{
    	name: "Leaky Brainpan" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-4 to social skill rolls you're off your nut rarely normal"
    }
	{
    	name: "Lightweight" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 on rolls to resist alcohol disease poison etc"
    }
	{
    	name: "Little Person" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+4 to ranged defenses -2 to movement actions speed reduced to 8 feet per turn"
    }
	{
    	name: "Memorable" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "+2 to rolls for others to spot or recognize you"
    }
	{
    	name: "Mute" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Must use non-verbal communications"
    }
	{
    	name: "Non-fightin' type" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Will only fight for your own survival -2 to combat rolls if forced to fight"
    }
	{
    	name: "Overconfident" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You always underestimate the opposition will bet it all on a roll of the dice pick fights you shouldn't etc"
    }
	{
    	name: "Paralyzed" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-4 to melee actions. Unassisted: Move 2 feet per turn Manual Wheelchair: Move 5 feet per turn -4 to movement actions Electric Wheelchair: Normal movement"
    }
	{
    	name: "Portly" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to all athletic rolls -2 to interaction rolls with fitness-focused individuals"
    }
	{
    	name: "Portly" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-4 to all athletic rolls -4 to interaction rolls with fitness-focused individuals -2 to all covert rolls involving disguise and hiding movement reduced to 5 feet per turn"
    }
	{
    	name: "Sadistic" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You're cruel and don't pass up the chance to hurt people"
    }
	{
    	name: "Scrawny" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to strength and athletic rolls -2 to interaction rolls with fitness-focused individuals"
    }
	{
    	name: "Soft" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "When taking any damage you take one more point of Stun. Must roll Average Willpower+Discipline to keep from weeping and wailing when Wounded"
    }
	{
    	name: "Stingy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "You never part with your money buy off brand haggle for everything"
    }
	{
    	name: "Straight Shooter" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to any attempt to lie/deceive"
    }
	{
    	name: "Superstitious" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to any roll if you feel you're having bad luck (bad omen happened) +2 to any roll for good luck (good omen happened)"
    }
	{
    	name: "Things Don't Go So Smooth" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Once per session GM can ask you to re-roll any roll and take lower result"
    }
	{
    	name: "Things Don't Go So Smooth" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Twice per session GM can ask you to re-roll any roll and take lower result"
    }
	{
    	name: "Traumatic Flashes" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Once per session GM can trigger incapable of action for d2 turns -2 to all attributes for 10 min"
    }
	{
    	name: "Traumatic Flashes" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Twice per session GM can trigger incapable of action for d2 turns -2 to all attributes for 10 min"
    }
	{
    	name: "Ugly as Sin" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "All relevant plot points spent cost twice the usual amount"
    }
	{
    	name: "Weak Stomach" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "-2 to all attributes until the source of the discomfort is removed"
    }
	{
    	name: "Weak Stomach" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: true
    	description: "Make an Average Vitality+Willpower roll to avoid fainting for 2d4 minutes every 5 minutes"
    }
	{
    	name: "Allergy" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to all physical skills in presence of chosen substance"
    }
	{
    	name: "Allergy" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Take d2 points of Stun each turn in presence of chosen substance for d4 turns (when your emergency injection kicks in)."
    }
	{
    	name: "Amputee" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to physical skills requiring 2 of chosen appendage base move reduced to 5 ft per turn if it's a leg and -4 to movement actions"
    }
	{
    	name: "Credo" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Inflexible principles - Mild consequences for following chosen credo (e.g. Always defend a lady never run from a fight)"
    }
	{
    	name: "Credo" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Inflexible principles - Dire consequences for following chosen credo (e.g. Don't leave a man behind Capt goes down with his ship)"
    }
	{
    	name: "Dull Sense" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to skill rolls involving the chosen sense"
    }
	{
    	name: "Hero Worship" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to social skills involving people who dislike your chosen hero"
    }
	{
    	name: "Loyal" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "Will not leave/betray the chosen target of your loyalty nor tolerate betrayal from others"
    }
	{
    	name: "Phobia" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to all attributes in the presence of chosen phobia"
    }
	{
    	name: "Prejudice" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to all interaction rolls with the chosen demographic"
    }
	{
    	name: "Slow Learner" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: true
    	description: "-2 to use of chosen Skill improvements cost 2 additional points to Skill or its Specialties"
    }
	{
    	name: "Absent Minded" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Easily distracted -2 to any activity when it's chaotic"
    }
	{
    	name: "Amnesia" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Block of time"
    }
	{
    	name: "Amnesia" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Identity/past"
    }
	{
    	name: "Amorous (over sexed)" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to social skills vs opposite sex to influence"
    }
	{
    	name: "Branded" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-4 to social skills where your reputation comes into play"
    }
	{
    	name: "Chip On Your Shoulder" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Anger Management Issues - Must make a willpower check when provoked or attack automatically. -2 to Willpower checks to avoid violence"
    }
	{
    	name: "Chip On Your Shoulder" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Anger Management Issues - -4 to Willpower checks to avoid violence and must make willpower check to avoid berserking when wounded"
    }
	{
    	name: "Cold As the Black" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "No emotions no connection with humanity -2 to social interactions"
    }
	{
    	name: "Combat Paralysis" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Must make willpower check after d2 rounds failure means you won't fight at all nearly catatonic"
    }
	{
    	name: "Compulsive Shopper" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "If somewhere shopping is possible MUST spend money"
    }
	{
    	name: "Coward" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 on combat actions when you're in danger"
    }
	{
    	name: "Coward" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-4 on combat actions must make willpower roll to avoid fleeing in panic when danger rears its ugly head"
    }
	{
    	name: "Dark Secret" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Humiliating secret from your past that haunts you"
    }
	{
    	name: "Dark Secret" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Criminal secret from your past that haunts you"
    }
	{
    	name: "Distractable" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Willpower -2 to stay focused"
    }
	{
    	name: "Ego Signature" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "You always leave a token or mark behind - +2 for others to track you easily framed"
    }
	{
    	name: "Forked Tongue" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "You lie all the time. Compelled to not tell the truth -4 to influence rolls due to your reputation -2 to group credibility rolls if you are present"
    }
	{
    	name: "Glass Jaw" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to Endurance"
    }
	{
    	name: "Glass Jaw" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-4 to Endurance"
    }
	{
    	name: "Glory Hound" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to Willpower roll to resist volunteering or going for glory"
    }
	{
    	name: "Good Samaritan" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Always help the underdog won't oppress or kick a downed foe"
    }
	{
    	name: "Greedy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Willpower roll required to avoid making really bad decision for money"
    }
	{
    	name: "Hooked" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Not overly dangerous (cigarettes functioning alcoholic) -2 to all attributes if you go longer than 2 days without a fix"
    }
	{
    	name: "Hooked" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Abusing a dangerous substance -4 to all attributes if you do not get a dose daily"
    }
	{
    	name: "Idealist" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to rolls to draw conclusions counter to your idealism"
    }
	{
    	name: "Illiterate Backbirth" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Can't read/write -2 to any tech skill"
    }
	{
    	name: "Illiterate Backbirth" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Afraid of technology superstitious"
    }
	{
    	name: "Illness (incurable persistent)" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Inconvenient -2 to physical skills"
    }
	{
    	name: "Illness (incurable persistent)" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Crippling -4 to physical skills"
    }
	{
    	name: "Lily Soft Hands" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 in all interactions with 'real men' who work for a living"
    }
	{
    	name: "Little Person" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 to ranged defenses -2 to movement actions and athletic skills"
    }
	{
    	name: "Memorable" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+2 to rolls for others to track you down -2 to attempts to disguise"
    }
	{
    	name: "Mute" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Must use non-verbal communications"
    }
	{
    	name: "Neatfreak/OCD" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Need things organized -2 when things are messy"
    }
	{
    	name: "Neatfreak/OCD" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Full-on OCD phobic of dirt -4 to any activity involving any kind of dirt"
    }
	{
    	name: "Paralyzed" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-4 to athletic and movement rolls -4 to melee combat"
    }
	{
    	name: "Portly" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to all athletic covert and movement rolls -2 to interaction rolls with fitness-focused individuals"
    }
	{
    	name: "Portly" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-4 to all athletic covert and movement rolls -4 to interaction rolls with fitness-focused individuals"
    }
	{
    	name: "Rebellious" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to any interaction with authority"
    }
	{
    	name: "Sadistic" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "You're cruel and need to hurt people. +2 to forced persuasion -2 in all 'proper' social situations -1 to the group due to your mannerisms"
    }
	{
    	name: "Second Class Citizen" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Shunned -2 to any social action"
    }
	{
    	name: "Second Class Citizen" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Untouchable -4 to any social action"
    }
	{
    	name: "Shy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to any action with someone you don't know"
    }
	{
    	name: "Slow Learner" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to any Lore skill roll because you didn't learn it as well as you thought"
    }
	{
    	name: "Soft" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "+1 vulnerability when you take melee dmg you take 1 more pt"
    }
	{
    	name: "Straight Shooter" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to any attempt to lie/deceive -1 to any group attempt if you are present"
    }
	{
    	name: "Superstitious" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to any roll if you feel you're having bad luck (bad omen happened)"
    }
	{
    	name: "Toes the Line" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Always follows the rules - -2 on actions that are illegal or against orders"
    }
	{
    	name: "Traumatic Flashes" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Twice per session GM can trigger -4 to all attributes for 10 min"
    }
	{
    	name: "Twitchy" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "You don't trust anyone -2 to skill rolls to influence"
    }
	{
    	name: "Two Left Feet" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to all physical tasks"
    }
	{
    	name: "Two Left Feet" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-4 to all physical tasks"
    }
	{
    	name: "Ugly as Sin" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to social skills interacting with others"
    }
	{
    	name: "Weak Stomach" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Make a willpower roll to avoid fainting -4 to attributes if you avoid fainting"
    }
	{
    	name: "Wisecraker/Smartass" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "-2 to willpower to resist mouthing off -2 to social interactions if failed"
    }
	{
    	name: "Young'un" 
    	level: "minor" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Under 20 treated as a kid"
    }
	{
    	name: "Young'un" 
    	level: "major" 
    	requiresSpecialization: false
    	doubleCost: false
    	original: false
    	description: "Under 16 underage restricted freedom"
    }
	{
    	name: "Allergy" 
    	level: "major" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "-4 to all physical skills in presence of chosen substance with an endurance check to avoid anaphylactic shock"
    }
	{
    	name: "Amputee" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "-2 to physical skills unable to do work requiring 2 of chosen appendage base move reduced if it's a leg"
    }
	{
    	name: "Phobia" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "-2 to all attributes in the presence of chosen phobia must make a willpower check to overcome"
    }
	{
    	name: "Prejudice" 
    	level: "minor" 
    	requiresSpecialization: true
    	doubleCost: false
    	original: false
    	description: "-4 to all interaction rolls with the chosen demographic -2 to all group interaction rolls if you're present"
    }
        
]