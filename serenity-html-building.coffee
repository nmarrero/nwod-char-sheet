getDNum = ( num ) ->
	if num < 14
		"d" + num 
	else
		num -= 12
		"d12 + " + num
		
generateStun = ( sibling, type, num, title ) ->
	for stun in [ 0...num + 1 ] by 1
		$input = $( "<input type='radio' />" )
		$span = $( "<span></span>" )
		$input.addClass( "sheet-serenity-#{type} sheet-serenity-#{type}#{stun}" )
		$input.val( stun )
		$input.attr( 'title', "#{stun} #{title}" )
		$input.attr( 'name', "attr_#{type}s" )
		$input.attr( 'title', "No #{title}\n(Click to clear)" ) if stun is 0
		$input.prop( 'checked', 'checked' ) if stun is 0
		$input.insertAfter( sibling )
		$span.insertAfter( $input )
		
_( $( 'assetsPlaceholder' ) ).each ( item ) ->
	$item = $( item )
	$selectTag = $( '<select></select>' )
	$selectTag.attr( 'class', $item.attr('class') )
	$selectTag.attr( 'name', $item.attr('name') )
	
	includeOriginals = true
	includeNonOriginals = true
	for asset in _( window.serenity.ASSETS ).sortBy( (asset) -> asset.name + if asset.level is "minor" then "0" else "1")
		continue if asset.original and not includeOriginals
		continue if not asset.original and not includeNonOriginals
		cost = if asset.level is "minor" then 2 else 4
		cost *=2 if asset.doubleCost
		asset_value = "#{asset.name} (#{asset.level}) - #{asset.description}"
		asset_description = "#{asset.name} (#{asset.level}) - Cost: #{cost} #{if asset.requiresSpecialization then ' - *Requires Specialization*' else ''}"
		$option = $( '<option></option' )
		$option.val( asset_value )
		$option.attr( 'title', asset.description )
		$option.html( asset_description )
		$selectTag.append( $option )
		
	$item.replaceWith( $selectTag )
	
_( $( 'complicationsPlaceholder' ) ).each ( item ) ->
	$item = $( item )
	$selectTag = $( '<select></select>' )
	$selectTag.attr( 'class', $item.attr('class') )
	$selectTag.attr( 'name', $item.attr('name') )
	
	includeOriginals = true
	includeNonOriginals = true
	for complication in _( window.serenity.COMPLICATIONS ).sortBy( (complication) -> complication.name + if complication.level is "minor" then "0" else "1")
		continue if complication.original and not includeOriginals
		continue if not complication.original and not includeNonOriginals
		cost = if complication.level is "minor" then 2 else 4
		cost *=2 if complication.doubleCost
		complication_value = "#{complication.name} (#{complication.level}) - #{complication.description}"
		complication_description = "#{complication.name} (#{complication.level}) - Cost: #{cost} #{if complication.requiresSpecialization then ' - *Requires Specialization*' else ''}"
		$option = $( '<option></option' )
		$option.val( complication_value )
		$option.attr( 'title', complication.description )
		$option.html( complication_description )
		$selectTag.append( $option )
		
	$item.replaceWith( $selectTag )
	
_( $( 'skillsPlaceholder' ) ).each ( item ) ->
	$item = $( item )
	$selectTag = $( '<select></select>' )
	$selectTag.attr( 'class', $item.attr('class') )
	$selectTag.attr( 'name', $item.attr('name') )
		
	for skill in window.serenity.SKILLS
		$option = $( '<option></option' )
		$option.val( skill )
		$option.html( skill )
		$selectTag.append( $option )
		
	$item.replaceWith( $selectTag )
	
_( $( 'stunPlaceholder' ) ).each ( item ) ->
	$item = $( item )
	type = $item.attr( 'type' )
	num = parseInt( $item.attr( 'num' ), 10 )
	title = $item.attr( 'title' )
	
	generateStun( $item, type, num, title )
	$item.remove()
	
_( $( 'dicePlaceholder' ) ).each ( item ) ->
	$item = $( item )
	defaultOpt = parseInt( $item.attr( 'default' ), 10 )
	start = parseInt( $item.attr( 'start' ), 10 )
	end = parseInt( $item.attr( 'end' ), 10 )

	$selectTag = $( '<select></select>' )
	$selectTag.attr( 'class', $item.attr('class') )
	$selectTag.attr( 'name', $item.attr('name') )
		
	for opt in [ start...end + 1 ] by 2
		$option = $( '<option></option' )
		$option.val( opt )
		$option.html( getDNum( opt ) )
		$option.prop( 'selected', true ) if opt is defaultOpt
		$selectTag.append( $option )
		
	$item.replaceWith( $selectTag )
	
_( $( 'damageTypePlaceholder' ) ).each ( item ) ->
	$item = $( item )

	$selectTag = $( '<select></select>' )
	$selectTag.attr( 'class', $item.attr('class') )
	$selectTag.attr( 'name', $item.attr('name') )
	
	strings = { 'B':'Basic', 'S':'Stun', 'W':'Wound' }
	for opt in [ 'B', 'S', 'W' ]
		$option = $( '<option></option' )
		$option.val( opt )
		$option.html( strings[opt] )
		$option.prop( 'selected', true ) if opt is 'B'
		$selectTag.append( $option )
		
	$item.replaceWith( $selectTag )